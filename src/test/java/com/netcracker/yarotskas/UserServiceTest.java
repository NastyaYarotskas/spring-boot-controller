package com.netcracker.yarotskas;

import com.netcracker.yarotskas.config.ServiceConfig;
import com.netcracker.yarotskas.entity.Item;
import com.netcracker.yarotskas.entity.User;
import com.netcracker.yarotskas.service.UserService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = ServiceConfig.class)
class UserServiceTest {

    @Autowired
    private UserService userService;

    private User testUser;

    private Item testItem;

    @BeforeEach
    void setUp() {
        testUser = new User();
        testUser.setName("Alex");

        testItem = new Item();
        testItem.setName("MacBook Pro");

        testUser.setItems(new ArrayList<>(Arrays.asList(testItem)));
    }

    @Test
    void testCreateSingleUser() {
        User createdUser = userService.create(testUser);
        assertNotNull(createdUser.getId());

    }

    @Test//
    void testUpdateSingleUser() {
        User createdUser = userService.create(testUser);
        createdUser.setName("Batman");
        User updatedUser = userService.update(createdUser);

        assertNotNull(updatedUser);
        assertEquals(createdUser.getId(), updatedUser.getId());
        assertEquals(createdUser.getName(), updatedUser.getName());
    }

    @Test
    void testDeleteSingleUser() {
        User createdUser = userService.create(testUser);

        Long id = createdUser.getId();
        userService.deleteById(id);

        assertNull(userService.findById(id));
    }

}
