package com.netcracker.yarotskas.config;


import com.netcracker.yarotskas.service.UserService;
import com.netcracker.yarotskas.service.impl.UserServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.yarotskas.service.impl")
public class ServiceConfig {
//    @Bean
//    public UserDao userDao() {
//        //return new UserDaoImpl();
//    }


//    @Bean
//    public UserService userService(UserDao userDao) {
//        return new UserServiceImpl(userDao);
//    }
}
