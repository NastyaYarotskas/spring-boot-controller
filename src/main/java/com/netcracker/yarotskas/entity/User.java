package com.netcracker.yarotskas.entity;


import com.netcracker.yarotskas.controller.dto.UserDto;
import org.springframework.context.annotation.Scope;

import javax.persistence.*;
import java.util.*;

@Entity
@Scope(value = "prototype")
@Table(name = "users")
public class User extends BaseEntity {

    private String name;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Item> items = new ArrayList<>();

    public User() {
    }

    public User(String name) {
        this.name = name;
    }

    private User(UserDto userDto) {
        this.name = userDto.getName();
        this.items = userDto.getItems();
    }

    public static User convertFromDto(UserDto userDto) {
        return new User(userDto);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(name, user.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                '}';
    }
}
