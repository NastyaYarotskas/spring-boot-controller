package com.netcracker.yarotskas.controller;

import com.netcracker.yarotskas.controller.dto.UserDto;
import com.netcracker.yarotskas.entity.User;
import com.netcracker.yarotskas.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "api/v1/users")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/{id}")
    public User findById(@PathVariable("id") Long id) {
        return userService.findById(id);
    }

    @GetMapping
    public Iterable<User> findAll() {
        return userService.findAll();
    }

    @PostMapping
    public User create(@RequestBody UserDto userDto) {
        return userService.create(User.convertFromDto(userDto));
    }

    @PutMapping
    public User update(@RequestBody User userToUpdate) {
        return userService.update(userToUpdate);
    }

    @DeleteMapping(path = "/delete_by_id/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        userService.deleteById(id);
    }
}
