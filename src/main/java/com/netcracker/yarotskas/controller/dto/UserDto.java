package com.netcracker.yarotskas.controller.dto;

import com.netcracker.yarotskas.entity.Item;
import lombok.Getter;

import java.util.List;

@Getter
public class UserDto {

    private String name;

    private List<Item> items;

}
