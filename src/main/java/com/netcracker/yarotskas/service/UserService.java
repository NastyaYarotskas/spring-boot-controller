package com.netcracker.yarotskas.service;

import com.netcracker.yarotskas.entity.User;

public interface UserService {

    User create(User user);

    User update(User user);

    User findById(Long id);

    void deleteById(Long id);

    Iterable<User> findAll();
}
